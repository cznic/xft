# xft

CGo-free port of libxft, the X FreeType library

## Installation

    $ go get modernc.org/xft

## Linking using ccgo

    $ ccgo foo.c -lmodernc.org/xft/lib

## Documentation

[godoc.org/modernc.org/xft](http://godoc.org/modernc.org/xft)

## Builders

[modern-c.appspot.com/-/builder/?importpath=modernc.org%2fxft](https://modern-c.appspot.com/-/builder/?importpath=modernc.org%2fxft)
