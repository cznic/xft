module modernc.org/xft

go 1.18

require (
	modernc.org/fontconfig v1.0.9
	modernc.org/freetype v1.0.8
	modernc.org/libc v1.22.6
	modernc.org/x11 v1.0.14
	modernc.org/xrender v1.0.8
)

require (
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	golang.org/x/sys v0.0.0-20220811171246-fbc7d0a398ab // indirect
	modernc.org/expat v1.0.9 // indirect
	modernc.org/gettext v0.0.14 // indirect
	modernc.org/mathutil v1.5.0 // indirect
	modernc.org/memory v1.5.0 // indirect
	modernc.org/xau v1.0.13 // indirect
	modernc.org/xcb v1.0.13 // indirect
	modernc.org/xdmcp v1.0.14 // indirect
	modernc.org/z v1.7.1 // indirect
)
